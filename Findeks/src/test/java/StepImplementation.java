import com.thoughtworks.gauge.Step;
import org.junit.Assert;

public class StepImplementation extends BaseTest {
    @Step("TEB anasayfasina gidilir")
    public void anaSayfayaGit() throws InterruptedException {
        getUrl();
        System.out.println("Anasayfa yuklendi");
    }

    @Step("<saniye> saniye bekle")
    public void waitElement(int key) throws InterruptedException {
        Thread.sleep(key * 1000);
        System.out.println(key + " saniye beklendi");
    }

    @Step("<key> uzerine gel bekle")
    public void elementHover(String key) {
        System.out.println(key+" geldi");
        hoverElement(key);
    }

    @Step("<key> tikla")
    public void elementTikla(String key) {
        clickElement(key);
        System.out.println(key + " tiklandi");
    }

    @Step("<key> elementi var mi")
    public void checkElement(String key) {
        try {
            findElement(key);
            System.out.println(key+ " elementi var");
        } catch (Exception e) {
            Assert.fail("Element bulunamadi.");
            System.out.println(key+ " elementi yok");
        }
    }

    @Step("Yeni sekme acildi")
    public void yeniSekme() {
        newtab();
        System.out.println("Yeni sekme acildi");
    }
}